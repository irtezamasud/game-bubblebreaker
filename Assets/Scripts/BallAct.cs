﻿
using UnityEngine;

public class BallAct : MonoBehaviour {

    public Vector2 startForce;
    public Rigidbody2D rb;                                   //Referencing the Rigidbody which is a physics 2D component
    public AudioClip jumpSound;

    public GameObject nextBall;                             // Referencing a gameobject where I put my own ball prefab as nextball element
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();                    // Getting the Component. If not, I need to do it manually
        rb.AddForce(startForce, ForceMode2D.Impulse);        // Add force to Rigidbody attached gameobject
	}


    public void Splite()                                     // A splite function, Where ball breaks into 2 pieces 
    {
        if(nextBall != null)
        {
            GameObject ball1 = Instantiate(nextBall, rb.position + Vector2.right /4f, Quaternion.identity);  //We use quaternion.identity when we dont any rotation
            GameObject ball2 = Instantiate(nextBall, rb.position + Vector2.left / 4f, Quaternion.identity);
            ball1.GetComponent<BallAct>().startForce = new Vector2(2f, 5f);                                   // setting the start force of splited ball
            ball2.GetComponent<BallAct>().startForce = new Vector2(-2f, 5f);

        }
        Destroy(gameObject);
        

    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "Wall")                                                                        //adding sound as soon as our balls hit wall
        {
            AudioSource.PlayClipAtPoint(jumpSound, transform.position);
        }
    }
    private void OnCollisionExit2D(Collision2D cols)
    {
        Destroy(jumpSound);
    }
}
