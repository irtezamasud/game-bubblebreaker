﻿using UnityEngine;

public class laserhit : MonoBehaviour {
    public Score scoreManage;

    private void Awake()                           //unity calls awake function when game just start
    {
        scoreManage = FindObjectOfType<Score>();
    }
    private void OnTriggerEnter2D(Collider2D col)  //This is a unity callback function where I can define my action while my collider is triggered
    {
        laser.IsFired = false;

        if(col.tag == "Ball")                       //finding collider with tag Ball. Name must be specific and precise
        {
            scoreManage.scoreCount += 2;
            col.GetComponent<BallAct>().Splite();  // Getting access in BallAct Script where I needed to use one of it's function 
        }
    }
}
