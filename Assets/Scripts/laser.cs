﻿using UnityEngine;

public class laser : MonoBehaviour {

    public static bool IsFired = false;                                                             // firing check
    public Transform player;
    public Animator myAnim;                                                                         // Reference the animator
    public AudioClip laserSound;                                                                    // initializing audio
    public float laserSpeed = 2f;

	// Use this for initialization
	void Start () {
        IsFired = false;
        myAnim.SetBool("isShoot", false);
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetButtonDown("Fire1"))
        {
            IsFired = true;
            
        }
            
        if (IsFired)
        {
            myAnim.SetBool("isShoot", true);
            AudioSource.PlayClipAtPoint(laserSound, transform.position);                            //playing audio at point
            transform.localScale = transform.localScale + Vector3.up * Time.deltaTime * laserSpeed; //moving the laser using vector3
        }else
        {
            myAnim.SetBool("isShoot", false);
            transform.position = player.position;
            transform.localScale = new Vector3(1f, 0f, 1f);                                         // reseting laser scale
        }
        Destroy(laserSound);                                                                        // Destroy the audioclip
	}
    
}
 