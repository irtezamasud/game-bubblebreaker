﻿
using UnityEngine;
using UnityEngine.SceneManagement;

public class playerControl : MonoBehaviour {

    public float speed = 10f;                                                       //setting player default speed. This is a good practice if we use f after float value
    public Rigidbody2D rb;
    
    private float move;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        
	}
	
	// Update is called once per frame
	void Update () {
        move = Input.GetAxisRaw("Horizontal");                                      //Getting input in right and left which is define as horizontal. Deleting Raw will make movement smooth. I didn't want smoothness in this case
	}

    void FixedUpdate()                                                              // Unity really likes to do all of it's physics action in fixedUpdate instead of Update funtion. Implementing it in update funtion will also work. But Fixed Update is just better than this 
    {
        rb.MovePosition (rb.position + new Vector2 (move * speed * Time.fixedDeltaTime, 0f) );     //moving the player according to my horizontal input
    }

    void OnCollisionEnter2D(Collision2D col)                                        //Action while my player collide with gameobject with tag Ball
    {
        if (col.collider.tag == "Ball")
        {
            gameObject.SetActive(false);                                            // player disable
            Debug.Log("Game Over!");                                                // throw a message in console window. It can be done better with using UI and adding some cool effects

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);       //Loading the active scene
        }
    }
}
