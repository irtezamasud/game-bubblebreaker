﻿
using UnityEngine;
using UnityEngine.UI;                       // this library gives us the power to use the Unity UI function

public class Score : MonoBehaviour {
    public Text scoreText;                  //Reference the text element for Score
    public Text HighscoreText;              //Reference the text element for HighScore
    public float scoreCount;
    public float highScoreCount;

	// Use this for initialization
	void Start () {
		if(PlayerPrefs.GetFloat("hiscore") != null)           //playerprefs is a function which can be used for saving game data in a computer memory.
        {
            highScoreCount = PlayerPrefs.GetFloat("hiscore");
        }
	}
	
	// Update is called once per frame
	void Update () {
        scoreText.text = "Score:  " + scoreCount;
        HighscoreText.text = "HighScore: " + highScoreCount;

        if(scoreCount > highScoreCount)                         // Highscore
        {
            highScoreCount = scoreCount;
            PlayerPrefs.SetFloat("hiscore", highScoreCount);
        }
	}
}
